\babel@toc {english}{}
\contentsline {part}{I\hspace {1em}Cybersecurity and normative initiatives}{3}{part.1}
\contentsline {section}{\numberline {1}Risk management}{5}{section.0.1}
\contentsline {section}{\numberline {2}Information security}{5}{section.0.2}
\contentsline {section}{\numberline {3}Critical infrastructure protection}{5}{section.0.3}
\contentsline {section}{\numberline {4}Wordwide initiatives}{5}{section.0.4}
\contentsline {part}{II\hspace {1em}Forensics}{7}{part.2}
\contentsline {section}{\numberline {5}Preliminaries}{9}{section.0.5}
\contentsline {subsection}{\numberline {5.1}Digital Evidence }{11}{subsection.0.5.1}
\contentsline {section}{\numberline {6}Forensics methodology}{12}{section.0.6}
\contentsline {subsection}{\numberline {6.1}NYCE Mexican norm/rule NMX-I-289-NYCE-2016}{12}{subsection.0.6.1}
\contentsline {subsubsection}{Identification stage}{12}{subsubsection*.3}
\contentsline {subsubsection}{Acquisition of tests or digital evidence}{13}{subsubsection*.4}
\contentsline {subsubsection}{Inspection and analysis of digital evidence}{14}{subsubsection*.5}
\contentsline {subsubsection}{Presentation of results and elaboration of the report}{15}{subsubsection*.6}
\contentsline {subsection}{\numberline {6.2}RFC 3227}{15}{subsection.0.6.2}
\contentsline {subsubsection}{Volatility Order}{16}{subsubsection*.7}
\contentsline {subsubsection}{Acquisition process}{17}{subsubsection*.8}
\contentsline {subsection}{\numberline {6.3}ISO/IEC 27037:2012}{17}{subsection.0.6.3}
\contentsline {subsection}{\numberline {6.4}Guide to Integrating Forensic Techniques into Incident Response - (NIST)}{20}{subsection.0.6.4}
\contentsline {section}{\numberline {7}Chain of Custody}{21}{section.0.7}
\contentsline {subsection}{\numberline {7.1}Processing}{22}{subsection.0.7.1}
\contentsline {subsection}{\numberline {7.2}Transfer}{24}{subsection.0.7.2}
\contentsline {subsection}{\numberline {7.3}Analysis}{24}{subsection.0.7.3}
\contentsline {subsection}{\numberline {7.4}Storage}{25}{subsection.0.7.4}
\contentsline {section}{\numberline {8}Study case}{25}{section.0.8}
\contentsline {part}{III\hspace {1em}Internet of things and embedded world}{33}{part.3}
\contentsline {section}{\numberline {9}Preliminaries}{35}{section.0.9}
\contentsline {section}{\numberline {10}Security in the IoT}{35}{section.0.10}
\contentsline {section}{\numberline {11}Cryptography in embedded devices}{35}{section.0.11}
\contentsline {section}{\numberline {12}Study cases}{35}{section.0.12}
\contentsline {part}{IV\hspace {1em}Data hiding}{37}{part.4}
\contentsline {section}{\numberline {13}Preliminaries}{39}{section.0.13}
\contentsline {section}{\numberline {14}Reversible data hiding in encrypted domain (RDH-ED)}{39}{section.0.14}
\contentsline {section}{\numberline {15}Classification of RDH-ED schemes}{42}{section.0.15}
\contentsline {subsection}{\numberline {15.1}Vacating Room Before Encryption (VRBE) }{42}{subsection.0.15.1}
\contentsline {subsection}{\numberline {15.2}Vacating Room After Encryption (VRAE) }{42}{subsection.0.15.2}
\contentsline {section}{\numberline {16}Study cases and application scenarios}{46}{section.0.16}
\contentsline {subsection}{\numberline {16.1}Protecting the sharing and distribution of color images hosted in cloud storage services}{47}{subsection.0.16.1}
\contentsline {subsection}{\numberline {16.2}Experimental results}{52}{subsection.0.16.2}
\contentsline {section}{\numberline {17}Future work}{60}{section.0.17}
\contentsline {part}{V\hspace {1em}Quantum-safe cryptography}{71}{part.5}
\contentsline {section}{\numberline {18}Preliminaries}{73}{section.0.18}
\contentsline {section}{\numberline {19}NIST Standarization process}{73}{section.0.19}
\contentsline {section}{\numberline {20}Classification of postquantum schemes}{75}{section.0.20}
\contentsline {subsection}{\numberline {20.1}Code-based Cryptography}{75}{subsection.0.20.1}
\contentsline {subsection}{\numberline {20.2}Lattice-based Cryptography}{76}{subsection.0.20.2}
\contentsline {subsection}{\numberline {20.3}Supersingular-Isogeneous-Elliptic-Curves-based Cryptography}{76}{subsection.0.20.3}
\contentsline {subsection}{\numberline {20.4}Hash-based Cryptography}{77}{subsection.0.20.4}
\contentsline {subsection}{\numberline {20.5}Multivariate-Quadratic-Equation-based Cryptography}{77}{subsection.0.20.5}
\contentsline {section}{\numberline {21}Quantum-safe cryptography in real world}{78}{section.0.21}
\contentsline {subsection}{\numberline {21.1}Quantum-safe Transport Layer Security}{78}{subsection.0.21.1}
\contentsline {subsection}{\numberline {21.2}Quantum-safe biometric schemes}{80}{subsection.0.21.2}
